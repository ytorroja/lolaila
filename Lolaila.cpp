//////////////////////////////////////////////////////////
// Placa de Fundamentos de Electrónica
#include <Arduino.h>
#include <Wire.h>
#include <Lolaila.h>

//////////////////////////////////////////////////////////
// Test Section
char rgbPin[3] = {6, 9, 5};

void setupRgb() {
  for(int i = 0; i < 3; i++) pinMode(rgbPin[i], OUTPUT);
  pinMode(11, OUTPUT);
}

void setRgb(unsigned char r, unsigned char g, unsigned char b) {
  analogWrite(rgbPin[0], r);
  analogWrite(rgbPin[1], g);
  analogWrite(rgbPin[2], b);
}

//////////////////////////////////////////////////////////
// Display Test Section
char segmentPin[7] = {16, 10, 8, 4, 7, 17, 12}; // a-g
char decodedPin[7]; // a-g
char digit[2];
char currDigit = 0;

#define controlPin 13

void setupDisplayPins() {
  for(int i = 0;  i < 7; i++) pinMode(segmentPin[i], OUTPUT);
  pinMode(controlPin, OUTPUT);
}

void setupDisplay() {
  setupDisplayPins();
  pinMode(11, OUTPUT);
}

char to7Seg(char n) {
  char res;
  switch(n) {
    
    case 0 : res = 0b01111110; break;
    case 1 : res = 0b00110000; break;
    case 2 : res = 0b01101101; break;
    case 3 : res = 0b01111001; break;
    case 4 : res = 0b00110011; break;
    case 5 : res = 0b01011011; break;
    case 6 : res = 0b01011111; break;
    case 7 : res = 0b01110000; break;
    case 8 : res = 0b01111111; break;
    case 9 : res = 0b01111011; break;
    
    default: res = 0b00000001; break;
  }
  
  // res = 0b00110000;
  
  return res;
}

void setDisplayDigit(char n, char val) {
  n = constrain(n, 0, 1);
  setDisplaySegments(0x00);
  digitalWrite(controlPin, n);
  char decodedOut = to7Seg(val);
  setDisplaySegments(decodedOut);
}

void setDisplaySegments(char n) {
  for(int i = 0; i < 7; i++) {
    digitalWrite(segmentPin[6-i], ((n >> i) & 0x01 != 0 ? HIGH : LOW));
  }
}

//////////////////////////////////////////////////////////
// Accelerometer Test Section
int  accelVal[3];

#define LOG_2_FIR_TAPS 4
#define FIR_TAPS 16 // ATTN!!!!!: Must be 2^^LOG_2_FIR_TAPS

int  AccFir[3][FIR_TAPS];
int  AccAccum[3];
uint8_t AccFirIdx[3];

int AccelAddress = 0x1D;

void accelInit() { 
  Wire.beginTransmission(AccelAddress);
  Wire.write(0x2A);  // Control register
  Wire.write(0x01);  // Active mode
  Wire.endTransmission();
  delay(5);
  /*
  Wire.beginTransmission(AccelAddress);
  Wire.send(0x31);  // Data format register
  Wire.send(0x08);  // set to full resolution
  Wire.endTransmission();
  delay(5);	
  // Because our main loop runs at 50Hz we adjust the output data rate to 50Hz (25Hz bandwidth)
  Wire.beginTransmission(AccelAddress);
  Wire.send(0x2C);  // Rate
  Wire.send(0x09);  // set to 50Hz, normal operation
  Wire.endTransmission();
  delay(5);
  */
}

void setupAccel() {
  Wire.begin();
  accelInit();
  pinMode(11, OUTPUT);
}

// Reads x,y and z accelerometer registers
void readAccelerometer() {
  int i = 0;
  byte buff[6];
  
  Wire.beginTransmission(AccelAddress); 
  Wire.write(0x01);       //sends address to read from
  Wire.endTransmission(); //end transmission
  
  Wire.beginTransmission(AccelAddress); //start transmission to device
  Wire.requestFrom(AccelAddress, 6);    // request 6 bytes from device
  
  // ((Wire.available())&&(i<6))
  while(Wire.available()) { 
    buff[i] = Wire.read();  // receive one byte
    i++;
  }
  
  Wire.endTransmission(); //end transmission
  
  // All bytes received?
  if (i==6) {
    accelVal[0] = (((int)buff[1]) << 8) | (buff[0]);    // Y axis (internal sensor x axis)
    accelVal[1] = (((int)buff[3]) << 8) | (buff[2]);    // X axis (internal sensor y axis)
    accelVal[2] = (((int)buff[5]) << 8) | (buff[4]);    // Z axis
    accelVal[0] >>= 6;
    accelVal[1] >>= 6;
    accelVal[2] >>= 6;
  } else
    Serial.println("!ERR: Acc data");
}

int getAccel(int axis) {
  readAccelerometer();
  int a = constrain(axis, 0, 2);
  return accelVal[a];
}

int getMeanAccel(int axis) {
  readAccelerometer();
  int a = constrain(axis, 0, 2);
  AccAccum[a] -= AccFir[a][AccFirIdx[a]];
  AccFir[a][AccFirIdx[a]] = accelVal[a];
  AccAccum[a] += AccFir[a][AccFirIdx[a]];
  AccFirIdx[a] = (AccFirIdx[a] + 1) & (FIR_TAPS - 1) ;

  return (AccAccum[a] >> LOG_2_FIR_TAPS);
}


//////////////////////////////////////////////////////////
// Tone
void toneDelay(int a, int b) {
  startTone(a);
  delay(b);
  stopTone();
}

//////////////////////////////////////////////////////////
// Miscelaneous
long int color1D(float c) {
  float s = 1.0 / 6;
  float R, G, B;
  if (c < s) {
    R = c / s;       G = 0;          B = 1;   
  } else if (c < 2 * s) {
    c = c - 1 * s;
    R = 1;           G = 0;          B = 1 - c / s;    
  } else if (c < 3 * s) {
    c = c - 2 * s;
    R = 1;           G = c / s;      B = 0;    
  } else if (c < 4 * s) {
    c = c - 3 * s;
    R = 1 - c / s;   G = 1;          B = 0;    
  } else if (c < 5 * s) {
    c = c - 4 * s;
    R = 0;           G = 1;          B = c / s;    
  } else {
    c = c -  5 * s;
    R = 0;           G = 1 - c / s;  B = 1;   
  }
  long int iR = (long int)constrain(R*255, 0, 255) << 16;
  long int iG = (long int)constrain(G*255, 0, 255) << 8;
  long int iB = (long int)constrain(B*255, 0, 255);
  return  iR | iG | iB;
}


