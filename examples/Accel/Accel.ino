//////////////////////////////////////////////////////////
// Test Placa de Fundamentos de Electrónica
#include <Wire.h>
#include <Lolaila.h>

void setup() {
  setupAccel();
}

int aX;
void loop() {  
  // Accel varies from -512 (-2g) to 512 (+2g)
  // aX = getAccel(X);
  aX = getMeanAccel(X);
  startTone(aX + 512);  
  delay(20);
}

