//////////////////////////////////////////////////////////
// Test Placa de Fundamentos de Electrónica
#include <Wire.h>
#include <Lolaila.h>

void setup() {
  setupRgb();
}

int i = 0;
void loop() {  
  //     R  G  B
  setRgb(i, 0, (255-i));
  i = i + 1;
  if (i > 255) i = 0;
  delay(20);
}

