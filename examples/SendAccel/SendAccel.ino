//////////////////////////////////////////////////////////
// Test Placa de Fundamentos de Electrónica
#include <Wire.h>
#include <Lolaila.h>

void setup() {
  setupAccel();
  Serial.begin(9600);
}

int aX;
void loop() {  
  aX = map(getAccel(X), -512, 512, 0, 255);
  Serial.write((char)aX);
  delay(20);
}

