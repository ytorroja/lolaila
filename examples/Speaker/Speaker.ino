//////////////////////////////////////////////////////////
// Test Placa de Fundamentos de Electr?nica
#include <Wire.h>
#include <Lolaila.h>

// Frecuencias de Do, Re, Mi, Fa, Sol, La, Si
int notes[7] = { 262, 294, 330, 349, 392, 440, 494 };

void setup() {
}

void loop() {  
  for(int i = 0; i < 7; i++) {
    playTone(notes[i], 100);
    delay(200);
  }
  playTone(notes[0] * 2, 500);  
  delay(1000);
}
