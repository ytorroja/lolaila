//////////////////////////////////////////////////////////
// Placa de Fundamentos de Electronica
#define startTone(A)    (A == 0 ? noTone(11) : tone(11, (A)))
#define stopTone()      noTone(11)
#define playTone(A, B)  (((A == 0) || (B == 0)) ? noTone(11) : toneDelay((A), (B)) )
#define X 1
#define Y 0
#define Z 2
#define getAccelX()     getAccel(X) 
#define getAccelY()     getAccel(Y) 
#define getAccelZ()     getAccel(Z) 
#define getMeanAccelX()     getMeanAccel(X) 
#define getMeanAccelY()     getMeanAccel(Y) 
#define getmeanAccelZ()     getMeanAccel(Z) 


//////////////////////////////////////////////////////////
// RGB Section
void setupRgb();
void setRgb(unsigned char r, unsigned char g, unsigned char b);

//////////////////////////////////////////////////////////
// Display Test Section
void setupDisplay();
void setDisplayDigit(char n, char val);
void setDisplaySegments(char n);

//////////////////////////////////////////////////////////
// Accelerometer Test Section
void setupAccel();
int getAccel(int axis);
int getMeanAccel(int axis);

//////////////////////////////////////////////////////////
// Tone
void toneDelay(int a, int b);

long int color1D(float c);
